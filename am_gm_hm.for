C       ARITHMETIC MEAN, GEOMETRIC MEAN AND HARMONIC MEAN OF THREE NUMBERS       
        PROGRAM MEANS
        REAL A, B, C
        A = 20.6
        B = 28.5
        C = 30.6
C       CALCULATING ARITHMETIC MEAN
        AM = (A + B +C)/3.0
C       CALCULATING GEOMETRIC MEAN
        GM = (A*B*C)**(1.0/3.0)
C       CALCULATING HARMONIC MEAN
        HM = 3.0/(1.0/A + 1.0/B + 1.0/C)
C       PRINTING VALUES OF ARITHMETIC, GEOMETRIC AND HARMONIC MEANS
        WRITE(*,*) 'The Arithmetic Mean is', AM
        WRITE(*,*) 'The Geometric Mean is', GM
        WRITE(*,*) 'The Harmonic Mean is', HM
        END