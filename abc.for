C       PROGRAM TO DO SOMETHING
100     PROGRAM ABC
        REAL A, B
        INTEGER C
C       GETTING INPUT
        READ *, A, B
C       DOING VARIOUS OPERATIONS
        C = A + B
        C = A * B
        C = A / B
        C = A ** B
        C = A ** 2
        C = A ** (1.0/2.0)
        C = SQRT(A)
C       PRINTING THE OUTPUT
        WRITE(*,*) C
200     END
